package com.Holidays.backoffice.Domain.GroupUser;

import com.Holidays.backoffice.Domain.User.User;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class GroupUser implements Serializable {

	private static final long serialVersionUID = -6736164754879384191L;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String groupName;
	
	@ManyToOne
	private User groupAdministrator;
	
	@OneToMany
	private List<User> groupUserList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public User getGroupAdministrator() {
		return groupAdministrator;
	}

	public void setGroupAdministrator(User groupAdministrator) {
		this.groupAdministrator = groupAdministrator;
	}

	public List<User> getGroupUserList() {
		return groupUserList;
	}

	public void setGroupUserList(List<User> groupUserList) {
		this.groupUserList = groupUserList;
	}

}