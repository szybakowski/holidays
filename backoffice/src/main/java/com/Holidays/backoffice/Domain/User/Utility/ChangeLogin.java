package com.Holidays.backoffice.Domain.User.Utility;

public class ChangeLogin {

    private String actualLogin;
    private String newLogin;

    public String getActualLogin() {
        return actualLogin;
    }

    public void setActualLogin(String actualLogin) {
        this.actualLogin = actualLogin;
    }

    public String getNewLogin() {
        return newLogin;
    }

    public void setNewLogin(String newLogin) {
        this.newLogin = newLogin;
    }
}
