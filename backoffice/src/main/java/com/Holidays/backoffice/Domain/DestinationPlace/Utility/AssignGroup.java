package com.Holidays.backoffice.Domain.DestinationPlace.Utility;

public class AssignGroup {

    private String groupName;
    private String place;

    public String getGroupName() {
        return groupName;
    }

    public String getPlace() {
        return place;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
