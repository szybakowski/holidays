package com.Holidays.backoffice.Domain.GroupUser.Utility;

public class AssignAdmin {

    private String groupName;
    private String login;

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getLogin() {
        return login;
    }

}
