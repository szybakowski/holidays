package com.Holidays.backoffice.Domain.Advertisement.Utility;

public class ChangeAdvertisementName {

    private String advertisementName;
    private String newAdvertisementName;

    public String getAdvertisementName() {
        return advertisementName;
    }

    public String getNewAdvertisementName() {
        return newAdvertisementName;
    }

    public void setAdvertisementName(String advertisementName) {
        this.advertisementName = advertisementName;
    }

    public void setNewAdvertisementName(String newAdvertisementName) {
        this.newAdvertisementName = newAdvertisementName;
    }
}
