package com.Holidays.backoffice.Domain.DestinationPlace;

import com.Holidays.backoffice.Domain.GroupUser.GroupUser;
import com.Holidays.backoffice.Domain.User.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class DestinationPlace implements Serializable {

	private static final long serialVersionUID = 2591698769403928998L;

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String place;
	
	@OneToMany																																																	
	private List<User>userList;
	
	@OneToMany
	private List<GroupUser> groupList;
	
	@Column
	private String tripTarget;
	
	
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<GroupUser> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<GroupUser> groupList) {
		this.groupList = groupList;
	}

	public String getTripTarget() {
		return tripTarget;
	}

	public void setTripTarget(String tripTarget) {
		this.tripTarget = tripTarget;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

}