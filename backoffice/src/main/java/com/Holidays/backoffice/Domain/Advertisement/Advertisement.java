package com.Holidays.backoffice.Domain.Advertisement;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Advertisement implements Serializable {

	private static final long serialVersionUID = 1944436109622854778L;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String place;
	
	@Column
	private String advertisementName;
	
	@Column
	private Date departureData;
	
	@Column
	private int numberOfPeopleRequired;
	
	@Column
	private String description;
	
	@Column
	private float amountUserCanSpend;

	public String getAdvertisementName() {
		return advertisementName;
	}

	public void setAdvertisementName(String advertisementName) {
		this.advertisementName = advertisementName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getDepartureData() {
		return departureData;
	}

	public void setDepartureData(Date departureData) {
		this.departureData = departureData;
	}

	public int getNumberOfPeopleRequired() {
		return numberOfPeopleRequired;
	}

	public void setNumberOfPeopleRequired(int numberOfPeopleRequired) {
		this.numberOfPeopleRequired = numberOfPeopleRequired;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getAmountUserCanSpend() {
		return amountUserCanSpend;
	}

	public void setAmountUserCanSpend(float amountUserCanSpend) {
		this.amountUserCanSpend = amountUserCanSpend;
	}

}