package com.Holidays.backoffice.Domain.DestinationPlace.Utility;

public class AssignUserDestiantion {

    private String login;
    private String place;

    public String getLogin() {
        return login;
    }

    public String getPlace() {
        return place;
    }

    public void setLogin(String login) {this.login = login; }

    public void setPlace(String place) {
        this.place = place;
    }
}
