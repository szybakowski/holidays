package com.Holidays.backoffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class BackofficeApplication extends SpringBootServletInitializer {


	@RequestMapping("/")
	public String hello() {
		return "asu123";
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BackofficeApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(BackofficeApplication.class, args);
	}
}

