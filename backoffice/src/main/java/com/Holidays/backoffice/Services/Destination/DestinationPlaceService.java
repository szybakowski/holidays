package com.Holidays.backoffice.Services.Destination;

import com.Holidays.backoffice.Domain.DestinationPlace.DestinationPlace;
import com.Holidays.backoffice.Domain.GroupUser.GroupUser;
import com.Holidays.backoffice.Repository.DestinationPlaceDao;
import com.Holidays.backoffice.Repository.GroupUserDao;
import com.Holidays.backoffice.Repository.UserDao;
import com.Holidays.backoffice.Domain.User.User;
import com.Holidays.backoffice.Utility.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DestinationPlaceService {

    @Autowired
    DestinationPlaceDao destinationPlaceDao;

    @Autowired
    UserDao userDao;

    @Autowired
    GroupUserDao groupUserDao;

    List<GroupUser> listOfGrupsThatWantGoToDestinationPlace = new ArrayList<GroupUser>();
    List<User> listOfUsersThatWantGoToDestinationPlace = new ArrayList<User>();

    public DataResponse createDestinationPlace(String place, String tripTarget) {
        DataResponse result = new DataResponse<User>();
        try {
            DestinationPlace destinationPlace = new DestinationPlace();
            destinationPlace.setPlace(place);
            destinationPlace.setTripTarget(tripTarget);
            destinationPlace = destinationPlaceDao.save(destinationPlace);

            List<DestinationPlace> newDestinationPlace = new ArrayList<DestinationPlace>();
            newDestinationPlace.add(destinationPlace);
            result.setData(newDestinationPlace);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse listDestinationPlace() {
        DataResponse result = new DataResponse<User>();
        try {
            List<DestinationPlace> listDestinationPlaces = new ArrayList<DestinationPlace>();
            Iterable<DestinationPlace> destinationPlaces = destinationPlaceDao.findAll();
            for (DestinationPlace destinationPlace : destinationPlaces)
                listDestinationPlaces.add(destinationPlace);
            result.setData(listDestinationPlaces);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse deleteDestinationPlace(String place) {
        DataResponse result = new DataResponse<User>();
        try {
            destinationPlaceDao.removeByPlace(place);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse assignUserToDestinationPlace(String login, String place) {
        DataResponse result = new DataResponse<User>();
        try {
            DestinationPlace destinationPlace = destinationPlaceDao.findByPlace(place);
            User user = userDao.findByLogin(login);
            listOfUsersThatWantGoToDestinationPlace.add(user);
            destinationPlace.setUserList(listOfUsersThatWantGoToDestinationPlace);
            destinationPlaceDao.save(destinationPlace);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse assignGroupToDestinationPlace(String groupName, String place) {
        DataResponse result = new DataResponse<User>();
        try {
            DestinationPlace destinationPlace = destinationPlaceDao.findByPlace(place);
            GroupUser groupUser = groupUserDao.findByGroupName(groupName);
            listOfGrupsThatWantGoToDestinationPlace.add(groupUser);
            destinationPlace.setGroupList(listOfGrupsThatWantGoToDestinationPlace);
            destinationPlaceDao.save(destinationPlace);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changeTripTarget(String place, String newTripTarget) {
        DataResponse result = new DataResponse<User>();
        try {
            DestinationPlace destinationPlace = destinationPlaceDao.findByPlace(place);
            destinationPlace.setTripTarget(newTripTarget);
            destinationPlaceDao.save(destinationPlace);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }
}