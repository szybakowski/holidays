package com.Holidays.backoffice.Services.User.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.CONFLICT, reason=" Users already exists. Login exist")
public class UserAlreadyExistException extends UserException{
	private static final long serialVersionUID = 1836137896933419953L;
}
