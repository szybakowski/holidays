package com.Holidays.backoffice.Services.GroupUser;

import com.Holidays.backoffice.Domain.GroupUser.GroupUser;
import com.Holidays.backoffice.Repository.GroupUserDao;
import com.Holidays.backoffice.Repository.UserDao;
import com.Holidays.backoffice.Domain.User.User;
import com.Holidays.backoffice.Services.GroupUser.exceptions.AlreadyExistException;
import com.Holidays.backoffice.Utility.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupUserService {

	@Autowired
	GroupUserDao groupUserDao;

	@Autowired
	UserDao userDao;

	List<User> listUsers = new ArrayList<User>();

	public DataResponse createGroupUser(String groupName) {
		DataResponse result = new DataResponse<User>();
		try {
			GroupUser groupUser = groupUserDao.findByGroupName(groupName);
			if (groupUser != null)
				throw new AlreadyExistException();
			groupUser = new GroupUser();
			groupUser.setGroupName(groupName);
			groupUser = groupUserDao.save(groupUser);

			List<GroupUser> newGroupUser = new ArrayList<GroupUser>();
			newGroupUser.add(groupUser);
			result.setData(newGroupUser);
			result.setStatus(HttpStatus.OK);
			result.setException("");

			return result;
		} catch (Exception e) {
			result.setStatus(HttpStatus.CONFLICT);
			result.setException(e.toString());

			return result;
		}
	}

	public DataResponse listGroupUser() {
		DataResponse result = new DataResponse<User>();
		try {
			List<GroupUser> listGroupUsers = new ArrayList<GroupUser>();
			Iterable<GroupUser> groupUsers = groupUserDao.findAll();
			for (GroupUser groupUser : groupUsers)
				listGroupUsers.add(groupUser);
			result.setData(listGroupUsers);
			result.setStatus(HttpStatus.OK);
			result.setException("");

			return result;
		} catch (Exception e) {
			result.setStatus(HttpStatus.CONFLICT);
			result.setException(e.toString());

			return result;
		}
	}

	public DataResponse deleteGroupUser(String groupName) {
		DataResponse result = new DataResponse<User>();
		try {
			groupUserDao.removeByGroupName(groupName);
			result.setStatus(HttpStatus.OK);
			result.setException("");

			return result;
		} catch (Exception e) {
			result.setStatus(HttpStatus.CONFLICT);
			result.setException(e.toString());

			return result;
		}
	}

	public DataResponse assignAdmin(String login, String groupName) {
		DataResponse result = new DataResponse<User>();
		try {
			GroupUser groupUser = groupUserDao.findByGroupName(groupName);
			User user = userDao.findByLogin(login);
			groupUser.setGroupAdministrator(user);
			groupUserDao.save(groupUser);
			result.setStatus(HttpStatus.OK);
			result.setException("");

			return result;
		} catch (Exception e) {
			result.setStatus(HttpStatus.CONFLICT);
			result.setException(e.toString());

			return result;
		}
	}

	public DataResponse assignUserToGroup(String login, String groupName) {
		DataResponse result = new DataResponse<User>();
		try {
			GroupUser groupUser = groupUserDao.findByGroupName(groupName);
			User user = userDao.findByLogin(login);
			listUsers.add(user);
			groupUser.setGroupUserList(listUsers);
			groupUserDao.save(groupUser);
			result.setStatus(HttpStatus.OK);
			result.setException("");

			return result;
		} catch (Exception e) {
			result.setStatus(HttpStatus.CONFLICT);
			result.setException(e.toString());

			return result;
		}
	}
}
