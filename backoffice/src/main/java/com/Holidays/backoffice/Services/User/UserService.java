package com.Holidays.backoffice.Services.User;

import com.Holidays.backoffice.Repository.UserDao;
import com.Holidays.backoffice.Domain.User.User;
import com.Holidays.backoffice.Utility.DataResponse;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

	@Autowired
	UserDao userDao;
	
	public DataResponse createUser(String login, String password, String firstName, String lastName, String email, String phone, String adress ) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			if (user != null) {
				response.setStatus(HttpStatus.CONFLICT);
				response.setException("Users already exists. Login exist");

				return response;
			}
			user = new User();
			user.setLogin(login);
			byte[] byteEncodePassword = Base64.encodeBase64(password.getBytes());
			String stringEncodePassword = new String(byteEncodePassword, StandardCharsets.UTF_8);
			user.setPassword(stringEncodePassword);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setPhone(phone);
			user.setAdress(adress);
			user = userDao.save(user);

			List<User> result = new ArrayList<User>();
			result.add(user);
			response.setData(result);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}
	
	public DataResponse listUser() {
		DataResponse response = new DataResponse<User>();
		try {
			List<User> result = new ArrayList<User>();
			Iterable<User> users = userDao.findAll();
			for (User user : users)
				result.add(user);
			response.setData(result);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}
	
	public DataResponse deleteUser(String login) {
		DataResponse response = new DataResponse<User>();
		try {
			userDao.removeByLogin(login);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}
	
	public DataResponse changeLogin(String login, String newLogin) {
		DataResponse response = new DataResponse<User>();
		try {
			if (login.equals("") || newLogin.equals("")) {
				response.setStatus(HttpStatus.CONFLICT);
				response.setException("ERROR REQUEST");
				return response;
			}
			User user = userDao.findByLogin(login);
			user.setLogin(newLogin);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}

	public DataResponse changePassword(String login, String newPassword) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			byte[] byteEncodePassword = Base64.encodeBase64(newPassword.getBytes());
			String stringEncodePassword = new String(byteEncodePassword, StandardCharsets.UTF_8);
			user.setPassword(stringEncodePassword);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}

	public DataResponse changeFirstName(String login, String newFirstName) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			user.setFirstName(newFirstName);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}

	public DataResponse changeLastName(String login, String newLastName) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			user.setLastName(newLastName);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}

	public DataResponse changeEmail(String login, String newEmail) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			user.setEmail(newEmail);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}

	public DataResponse changePhone(String login, String newPhone) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			user.setPhone(newPhone);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}

	public DataResponse changeAdress(String login, String newAdress) {
		DataResponse response = new DataResponse<User>();
		try {
			User user = userDao.findByLogin(login);
			user.setAdress(newAdress);
			userDao.save(user);
			response.setStatus(HttpStatus.OK);
			response.setException("");

			return response;
		} catch (Exception e) {
			response.setStatus(HttpStatus.CONFLICT);
			response.setException(e.toString());

			return response;
		}
	}
}
