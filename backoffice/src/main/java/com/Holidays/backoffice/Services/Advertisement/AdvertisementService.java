package com.Holidays.backoffice.Services.Advertisement;

import com.Holidays.backoffice.Domain.Advertisement.Advertisement;
import com.Holidays.backoffice.Repository.AdvertisementDao;
import com.Holidays.backoffice.Utility.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AdvertisementService {

    @Autowired
    AdvertisementDao advertisementDao;

    public DataResponse createAdvertisement(String place, String advertisementName, Date departureData, int numberOfPeopleRequired, String description, float amountUserCanSpend) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = new Advertisement();
            advertisement.setPlace(place);
            advertisement.setAdvertisementName(advertisementName);
            advertisement.setDepartureData(departureData);
            advertisement.setNumberOfPeopleRequired(numberOfPeopleRequired);
            advertisement.setDescription(description);
            advertisement.setAmountUserCanSpend(amountUserCanSpend);
            advertisement = advertisementDao.save(advertisement);

            List<Advertisement> newAdvertisement = new ArrayList<Advertisement>();
            newAdvertisement.add(advertisement);
            result.setData(newAdvertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse listAdvertisement() {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            List<Advertisement> listAdvertisement = new ArrayList<Advertisement>();
            Iterable<Advertisement> advertisements = advertisementDao.findAll();
            for (Advertisement advertisement : advertisements)
                listAdvertisement.add(advertisement);
            result.setData(listAdvertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse deleteAdvertisement(String advertisementName) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            advertisementDao.removeByAdvertisementName(advertisementName);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changePlace(String advertisementName, String place) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = advertisementDao.findByAdvertisementName(advertisementName);
            advertisement.setPlace(place);
            advertisementDao.save(advertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changeAdvertisementName(String advertisementName, String newAdvertisementName) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = advertisementDao.findByAdvertisementName(advertisementName);
            advertisement.setAdvertisementName(newAdvertisementName);
            advertisementDao.save(advertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changeDepartureData(String advertisementName, Date departureData) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = advertisementDao.findByAdvertisementName(advertisementName);
            advertisement.setDepartureData(departureData);
            advertisementDao.save(advertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changeNumberOfPeopleRequired(String advertisementName, int numberOfPeopleRequired) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = advertisementDao.findByAdvertisementName(advertisementName);
            advertisement.setNumberOfPeopleRequired(numberOfPeopleRequired);
            advertisementDao.save(advertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changeDescription(String advertisementName, String description) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = advertisementDao.findByAdvertisementName(advertisementName);
            advertisement.setDescription(description);
            advertisementDao.save(advertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }

    public DataResponse changeAmountUserCanSpend(String advertisementName, float amountUserCanSpend) {
        DataResponse result = new DataResponse<Advertisement>();
        try {
            Advertisement advertisement = advertisementDao.findByAdvertisementName(advertisementName);
            advertisement.setAmountUserCanSpend(amountUserCanSpend);
            advertisementDao.save(advertisement);
            result.setStatus(HttpStatus.OK);
            result.setException("");

            return result;
        } catch (Exception e) {
            result.setStatus(HttpStatus.CONFLICT);
            result.setException(e.toString());

            return result;
        }
    }
}
