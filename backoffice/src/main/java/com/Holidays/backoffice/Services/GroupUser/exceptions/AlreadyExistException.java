package com.Holidays.backoffice.Services.GroupUser.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.CONFLICT, reason="Group Users already exists. Cannot create new one.")
public class AlreadyExistException extends GruopUserException {
	private static final long serialVersionUID = -8835631512345034107L;
}