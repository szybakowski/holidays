package com.Holidays.backoffice.Controller;

import com.Holidays.backoffice.Domain.GroupUser.GroupUser;
import com.Holidays.backoffice.Domain.GroupUser.Utility.AssignAdmin;
import com.Holidays.backoffice.Domain.GroupUser.Utility.AssignUser;
import com.Holidays.backoffice.Services.GroupUser.GroupUserService;
import com.Holidays.backoffice.Utility.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class GroupUserController {

    @Autowired
    GroupUserService groupUserService;

    @GetMapping ("/group_users")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse groupUser(){
        return groupUserService.listGroupUser();
    }

    @PostMapping("/group_user")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse createGroupUser(@RequestBody GroupUser groupUser) {
        return groupUserService.createGroupUser(groupUser.getGroupName());
    }

    @PutMapping("/assign_admin_group")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse assignAdmin(@RequestBody AssignAdmin admin) {
        return groupUserService.assignAdmin(admin.getLogin(), admin.getGroupName());
    }

    @PutMapping("/assign_user_group")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse assignUserToGroup(@RequestBody AssignUser user) {
        return groupUserService.assignUserToGroup(user.getLogin(), user.getGroupName());
    }

    @DeleteMapping("/delete_group")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse removeGroupUser(@RequestBody GroupUser groupUser) {
        return groupUserService.deleteGroupUser(groupUser.getGroupName());
    }
}