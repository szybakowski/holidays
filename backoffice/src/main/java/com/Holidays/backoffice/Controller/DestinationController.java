package com.Holidays.backoffice.Controller;

import com.Holidays.backoffice.Domain.DestinationPlace.DestinationPlace;
import com.Holidays.backoffice.Domain.DestinationPlace.Utility.AssignGroup;
import com.Holidays.backoffice.Domain.DestinationPlace.Utility.AssignUserDestiantion;
import com.Holidays.backoffice.Services.Destination.DestinationPlaceService;
import com.Holidays.backoffice.Utility.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestinationController {

    @Autowired
    DestinationPlaceService destinationPlaceService;

    @GetMapping("/destination_places")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse destinationPlace(){
        return destinationPlaceService.listDestinationPlace();
    }

    @PostMapping("/destination_place/")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse createDestinationPlace(@RequestBody DestinationPlace destinationPlace) {
        return destinationPlaceService.createDestinationPlace(destinationPlace.getPlace(), destinationPlace.getTripTarget());
    }

    @PutMapping("/add_user_want_go_destination_place")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse assignUserToDestinationPlace(@RequestBody AssignUserDestiantion user) {
        return destinationPlaceService.assignUserToDestinationPlace(user.getLogin(), user.getPlace());
    }

    @PutMapping("/add_group_want_go_destination_place")
    public DataResponse assignGroupToDestinationPlace(@RequestBody AssignGroup group) {
        return destinationPlaceService.assignGroupToDestinationPlace(group.getGroupName(), group.getPlace());
    }

    @PutMapping("/change_destination_place_triptarget/")
    public DataResponse changeTripTarget(@RequestBody DestinationPlace destinationPlace) {
       return destinationPlaceService.changeTripTarget(destinationPlace.getPlace(), destinationPlace.getTripTarget());
    }

    @DeleteMapping("/delete_ destination_place")
    public DataResponse removeDestinationPlace(@RequestBody DestinationPlace destinationPlace) {
        return destinationPlaceService.deleteDestinationPlace(destinationPlace.getPlace());
    }
}
