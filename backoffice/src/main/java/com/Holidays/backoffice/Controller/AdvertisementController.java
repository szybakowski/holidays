package com.Holidays.backoffice.Controller;

import com.Holidays.backoffice.Domain.Advertisement.Advertisement;
import com.Holidays.backoffice.Domain.Advertisement.Utility.ChangeAdvertisementName;
import com.Holidays.backoffice.Services.Advertisement.AdvertisementService;
import com.Holidays.backoffice.Utility.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class AdvertisementController {

    @Autowired
    AdvertisementService advertisementService;

    @GetMapping("/advertisements")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse advertisement(){
        return advertisementService.listAdvertisement();
    }

    @PostMapping("/advertisement")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse createAdvertisement(@RequestBody Advertisement advertisement) {
        return advertisementService.createAdvertisement(advertisement.getPlace(), advertisement.getAdvertisementName(), advertisement.getDepartureData(), advertisement.getNumberOfPeopleRequired(), advertisement.getDescription(), advertisement.getAmountUserCanSpend() );
    }

    @PutMapping("/change_advertisement_place")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changePlace(@RequestBody Advertisement advertisement) {
        return advertisementService.changePlace(advertisement.getAdvertisementName(), advertisement.getPlace());
    }

    @PutMapping("/change_advertisement_name")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeAdvertisementName(@RequestBody ChangeAdvertisementName advertisement) {
        return advertisementService.changeAdvertisementName(advertisement.getAdvertisementName(), advertisement.getNewAdvertisementName());
    }

    @PutMapping("/change_advertisement_departure_date")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeDepartureData(@RequestBody Advertisement advertisement) {
        return advertisementService.changeDepartureData(advertisement.getAdvertisementName(), advertisement.getDepartureData());
    }

    @PutMapping("/change_advertisement_ number_people_required")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeNumberOfPeopleRequired(@RequestBody Advertisement advertisement) {
        return advertisementService.changeNumberOfPeopleRequired(advertisement.getAdvertisementName(), advertisement.getNumberOfPeopleRequired());
    }

    @PutMapping("/change_advertisement_description")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeDescription(@RequestBody Advertisement advertisement) {
        return advertisementService.changeDescription(advertisement.getAdvertisementName(), advertisement.getDescription());
    }

    @PutMapping("/change_advertisement_amount")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeAmountUserCanSpend(@RequestBody Advertisement advertisement) {
        return advertisementService.changeAmountUserCanSpend(advertisement.getAdvertisementName(), advertisement.getAmountUserCanSpend());
    }

    @DeleteMapping("/delete_advertisement")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse removeAdvertisement(@RequestBody Advertisement advertisement) {
        return advertisementService.deleteAdvertisement(advertisement.getAdvertisementName());
    }
}