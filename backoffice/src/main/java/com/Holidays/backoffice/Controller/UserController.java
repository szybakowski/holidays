package com.Holidays.backoffice.Controller;

import com.Holidays.backoffice.Repository.UserDao;
import com.Holidays.backoffice.Domain.User.User;
import com.Holidays.backoffice.Domain.User.Utility.ChangeLogin;
import com.Holidays.backoffice.Domain.User.Utility.ChangePassword;
import com.Holidays.backoffice.Services.User.UserService;
import com.Holidays.backoffice.Utility.DataResponse;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    @GetMapping("/users")
    @ResponseStatus(HttpStatus.OK)
    DataResponse user(){
        List<User> result = new ArrayList<User>();
        DataResponse response = userService.listUser();
        return response;
    }

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse createUser(@RequestBody User user) {
        DataResponse response = userService.createUser(
                user.getLogin(),
                user.getPassword(),
                user.getFirstName(),
                user.getLastName(), user.getEmail(),
                user.getPhone(),
                user.getAdress());
        return response;
    }

    @PutMapping("/change_login")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeLogin(@RequestBody ChangeLogin login) {
        return userService.changeLogin(login.getActualLogin(), login.getNewLogin());
    }

    @PutMapping("/change_password")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changePassword(@RequestBody ChangePassword passoword) {
        return userService.changePassword(passoword.getLogin(), passoword.getNewPassword());
    }

    @PutMapping("/change_firstname")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeFirstName(@RequestBody User first) {
        return userService.changeFirstName(first.getLogin(), first.getFirstName());
    }

    @PutMapping("/change_lastname")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeLastName(@RequestBody User lastName) {
        return userService.changeLastName(lastName.getLogin(), lastName.getLastName());
    }

    @PutMapping("/change_email")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeEmail(@RequestBody User email) {
        return userService.changeEmail(email.getLogin(), email.getEmail());
    }

    @PutMapping("/change_phone")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changePhone(@RequestBody User phone) {
        return userService.changePhone(phone.getLogin(), phone.getPhone());
    }

    @PutMapping("/change_addres")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse changeAdress(@RequestBody User phone) {
        return userService.changeAdress(phone.getLogin(), phone.getAdress());
    }

    @DeleteMapping("/delete_login")
    @ResponseStatus(HttpStatus.OK)
    public DataResponse removeUser(@RequestBody User login) {
        return userService.deleteUser(login.getLogin());
    }
}
