package com.Holidays.backoffice.Repository;

import com.Holidays.backoffice.Domain.GroupUser.GroupUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface GroupUserDao extends CrudRepository<GroupUser, Long>  {

	GroupUser findByGroupName(String groupName);
	
	@Transactional
	List<GroupUser> removeByGroupName(String groupName);
}
