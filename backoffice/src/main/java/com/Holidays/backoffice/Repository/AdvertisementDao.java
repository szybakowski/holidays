package com.Holidays.backoffice.Repository;

import com.Holidays.backoffice.Domain.Advertisement.Advertisement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface AdvertisementDao extends CrudRepository<Advertisement, Long> {
	
	Advertisement findByAdvertisementName(String advertisementName);
	
	@Transactional
	List<Advertisement> removeByAdvertisementName(String advertisementName);

}
