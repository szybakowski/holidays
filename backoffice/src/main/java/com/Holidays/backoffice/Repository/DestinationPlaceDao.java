package com.Holidays.backoffice.Repository;

import com.Holidays.backoffice.Domain.DestinationPlace.DestinationPlace;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DestinationPlaceDao extends CrudRepository<DestinationPlace, Long> {

	DestinationPlace findByPlace(String place);
	
	@Transactional
	List<DestinationPlace> removeByPlace(String place);
}