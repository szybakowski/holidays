package com.Holidays.backoffice.Repository;

import com.Holidays.backoffice.Domain.User.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserDao extends CrudRepository<User, Long> {

	User findByLogin(String login);
	
	@Transactional
	List<User> removeByLogin(String login);
}
