package com.Holidays.backoffice.Utility;

import org.springframework.http.HttpStatus;

import java.util.List;

public class DataResponse<T> {

    private List<T> data;
    private String exception;
    private HttpStatus status;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}